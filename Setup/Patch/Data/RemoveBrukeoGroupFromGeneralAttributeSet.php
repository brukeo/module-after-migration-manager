<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\AfterMigrationManager\Setup\Patch\Data;

class RemoveBrukeoGroupFromGeneralAttributeSet implements \Magento\Framework\Setup\Patch\DataPatchInterface
{

    protected \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup;
    protected \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory;
    protected \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetcollectionFactory;

    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetcollectionFactory
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeSetcollectionFactory = $attributeSetcollectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $attributeSetId = $this->getAttributeSetId($eavSetup);
        $eavSetup->removeAttributeGroup(
            \Magento\Catalog\Model\Product::ENTITY,
            $attributeSetId,
            'Brukeo'
        );

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    protected function getAttributeSetId(\Magento\Eav\Setup\EavSetup $eavSetup): int
    {
        $entityTypeId = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
        /** @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\Collection $attributeSets */
        $attributeSets = $this->attributeSetcollectionFactory->create()
            ->addFieldToSelect('*')
            ->addFieldToFilter('attribute_set_name', 'Default')
            ->addFieldToFilter('entity_type_id', $entityTypeId);

        return $attributeSets->getFirstItem()->getData('attribute_set_id');
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

}

