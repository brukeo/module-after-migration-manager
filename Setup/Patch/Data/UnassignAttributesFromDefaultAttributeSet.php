<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\AfterMigrationManager\Setup\Patch\Data;

class UnassignAttributesFromDefaultAttributeSet implements \Magento\Framework\Setup\Patch\DataPatchInterface
{

    protected array $attributeCodesToUnassign = [
        'sku_pattern', 'sku_phase', 'sku_finish', 'sku_corners'
    ];

    protected \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup;
    protected \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory;
    protected \Magento\Eav\Api\AttributeManagementInterface $attributeManagement;
    protected \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetcollectionFactory;

    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory,
        \Magento\Eav\Api\AttributeManagementInterface $attributeManagement,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetcollectionFactory
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeManagement = $attributeManagement;
        $this->attributeSetcollectionFactory = $attributeSetcollectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $attributeSetId = $this->getAttributeSetId($eavSetup);
        foreach ($this->attributeCodesToUnassign as $attributeCodesToUnassign) {
            $this->attributeManagement->unassign($attributeSetId, $attributeCodesToUnassign);
        }

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [
            \Brukeo\DistributorsManager\Setup\Patch\Data\UpdateDistributorsOfferAttributeIsUserDefined::class,
            \Brukeo\DistributorsManager\Setup\Patch\Data\AssignDistributorAttributesToDistributorAttributeSet::class,
            \Brukeo\ColorAttributeManager\Setup\Patch\Data\AddColorSwatchAttribute::class
        ];
    }

    protected function getAttributeSetId(\Magento\Eav\Setup\EavSetup $eavSetup): int
    {
        $entityTypeId = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
        /** @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\Collection $attributeSets */
        $attributeSets = $this->attributeSetcollectionFactory->create()
            ->addFieldToSelect('*')
            ->addFieldToFilter('attribute_set_name', 'Default')
            ->addFieldToFilter('entity_type_id', $entityTypeId);

        return $attributeSets->getFirstItem()->getData('attribute_set_id');
    }

}
