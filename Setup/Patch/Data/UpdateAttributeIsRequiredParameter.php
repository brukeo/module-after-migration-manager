<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\AfterMigrationManager\Setup\Patch\Data;

class UpdateAttributeIsRequiredParameter implements \Magento\Framework\Setup\Patch\DataPatchInterface
{

    protected array $attributeCodes = [
        'sku_pattern', 'sku_phase', 'sku_finish', 'sku_corners', 'category_group_pwf', 'category_group_ksb',
        'category_group_ptw', 'category_group_kio', 'category_group_pdy', 'category_group_sse', 'category_group_ogr_mro',
        'category_group_maa', 'category_group_sie', 'category_group_pru', 'dimensions_ptw',
        'type_kio', 'type_sse', 'type_ogr_mro', 'type_maa', 'type_sie', 'type_pru', 'element_ogr', 'dimensions_sse',
        'type_pdy', 'dimensions_pdy', 'dimensions_pwf', ''
    ];

    protected \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup;

    public function __construct(\Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup)
    {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        foreach ($this->attributeCodes as $attributeCode) {
            $this->moduleDataSetup->getConnection()->query(
                "UPDATE eav_attribute SET is_required = :isRequired WHERE attribute_code = :attributeCode",
                ['isRequired' => 0, 'attributeCode' => $attributeCode]
            );
        }

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

}
