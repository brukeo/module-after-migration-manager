<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Brukeo_AfterMigrationManager',
    __DIR__
);